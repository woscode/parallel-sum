package com.teamzero;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParallelCalculation {

    private List<Integer> listOfNumbers = null;
    private Integer numbers = 0;
    private Integer threads = 1;
    private Long sum = 0L;

    public ParallelCalculation(List<Integer> listOfNumbers, int numbers, int threads, int range) {

        this.numbers = numbers < 0 ? 0 : numbers;

        if (threads < 1 || threads > numbers)
            this.threads = numbers > Runtime.getRuntime().availableProcessors() ?
                    Runtime.getRuntime().availableProcessors() : 1;
        else
            this.threads = threads;

        if (range <= 0)
            range = 1;

        this.listOfNumbers = listOfNumbers;

        if (Objects.isNull(this.listOfNumbers) || this.listOfNumbers.size() != this.numbers ) {
            this.listOfNumbers = new ArrayList<>(this.numbers);
            Random random = new Random();
            for (int i = 0; i < this.numbers; i++)
                this.listOfNumbers.add(random.nextInt() % range);
        }
    }

    public long sum()  {

        int partition = (int) Math.ceil((double) numbers / threads);

        for (int i = threads-1; i >= 0; i--) {
            int from = i * partition;
            int to = Math.min((i + 1) * partition, listOfNumbers.size());
            new Thread(() -> {
                long partialSum = 0L;
                for (int j = from; j < to; j++)
                    partialSum += listOfNumbers.get(j);
                accumulate(partialSum);
            }).start();
        }
        return getSum();
    }

    private synchronized Long getSum() {
        try {
            while (threads > 0)
                wait();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        return sum;
    }

    private synchronized void accumulate(long partialSum) {
        threads--;
        sum += partialSum;
        notifyAll();
    }

    public static void main(String[] args) {

    	Logger.getGlobal().setLevel(Level.INFO);
    	
        Scanner in = new Scanner(System.in);

        Logger.getGlobal().info("Enter amount of numbers:");
        int numbers = in.nextInt();

        Logger.getGlobal().info("Enter number of threads:");
        int threads = in.nextInt();

        long computationTime = System.currentTimeMillis();

        ParallelCalculation parallelCalculation = new ParallelCalculation(null, numbers, threads, 1000);
        long psum = parallelCalculation.sum();
        
        computationTime = System.currentTimeMillis() - computationTime;
        
        Logger.getGlobal().log(Level.INFO, "Parallel sum = {0}", psum);
        Logger.getGlobal().log(Level.INFO, "Time: {0} ms.", computationTime);
        
        computationTime = System.currentTimeMillis();
        
        long lsum = parallelCalculation.listOfNumbers.stream().mapToLong(Integer::longValue).sum();
        
        computationTime = System.currentTimeMillis() - computationTime
        		;
        Logger.getGlobal().log(Level.INFO, "Linear sum = {0}", lsum);
        Logger.getGlobal().log(Level.INFO, "Time: {0} ms.", computationTime);
    }
}
