package com.teamzero;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JoinParallelSum {

    private List<Integer> listOfNumbers = null;
    private int numbers = 0;
    private int threads = 1;

    JoinParallelSum(List<Integer> listOfNumbers, int numbers, int threads, int range) {

        this.numbers = numbers < 0 ? 0 : numbers;

        if (threads < 1 || threads > numbers)
            this.threads = numbers > Runtime.getRuntime().availableProcessors() ?
                    Runtime.getRuntime().availableProcessors() : 1;
        else
            this.threads = threads;

        this.listOfNumbers = listOfNumbers;

        if (Objects.isNull(this.listOfNumbers) || this.listOfNumbers.size() != this.numbers ) {
            this.listOfNumbers = new ArrayList<>(this.numbers);
            Random random = new Random();
            for (int i = 0; i < this.numbers; i++)
                this.listOfNumbers.add(random.nextInt() % range);
        }
    }

    public long calculate() {

        int partition = (int) Math.ceil((double) numbers / threads);
        PartialSumCalculation[] tasks = new PartialSumCalculation[threads]; // наследуется от класса Thread

        for (int i = 0; i < threads; i++) {
            tasks[i] = new PartialSumCalculation(i * partition, (i + 1) * partition);
            tasks[i].start();
        }

        try {
            for (PartialSumCalculation task : tasks)
                task.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }

        long totalSum = 0;
        for (int i = 0; i < tasks.length; i++)
            totalSum += tasks[i].partialsum;

        return totalSum;
    }

    class PartialSumCalculation extends Thread {

        private long partialsum = 0L;
        private int from;
        private int to;

        public PartialSumCalculation(int from, int to) {
            this.from = from;
            this.to = to < listOfNumbers.size() ? to : listOfNumbers.size() ;
        }

        @Override
        public void run() {
            for (; from < to; from++)
            	partialsum += listOfNumbers.get(from);
        }
    }

    public static void main(String[] args) {

    	Logger.getGlobal().setLevel(Level.INFO);
    	
        Scanner in = new Scanner(System.in);

        Logger.getGlobal().info("Enter amount of numbers:");
        int numbers = in.nextInt();

        Logger.getGlobal().info("Enter number of threads:");
        int threads = in.nextInt();

        long computationTime = System.currentTimeMillis();

        JoinParallelSum joinPartialSum = new JoinParallelSum(null, numbers, threads, 1000);
        long psum = joinPartialSum.calculate();
        
        computationTime = System.currentTimeMillis() - computationTime;
        
        Logger.getGlobal().log(Level.INFO, "Parallel sum = {0}", psum);
        Logger.getGlobal().log(Level.INFO, "Time: {0} ms.", computationTime);
        
        computationTime = System.currentTimeMillis();
        
        long lsum = joinPartialSum.listOfNumbers.stream().mapToLong(Integer::longValue).sum();
        
        computationTime = System.currentTimeMillis() - computationTime
        		;
        Logger.getGlobal().log(Level.INFO, "Linear sum = {0}", lsum);
        Logger.getGlobal().log(Level.INFO, "Time: {0} ms.", computationTime);
    }
}

